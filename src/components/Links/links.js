import React from 'react';
import config from 'visual-config-exposer';
import styled, { css } from 'styled-components';

import './links.css';

const links = config.settings.customLinks;
console.log(links);
const transparency = config.settings.transparency;
const borderColor = config.settings.borderClr;
const rowColor = config.settings.rowClr;

const Link = styled.a`
  height: 50px;
  width: 500px;
  margin: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 15px;
  box-shadow: 2px 5px 10px rgb(31, 30, 30);
  cursor: pointer;
  transition: all 0.3s ease;
  color: ${(props) => (props.transparency ? 'black' : 'white')};
  text-decoration: none;
  background-color: ${(props) =>
    props.transparency
      ? 'rgba(0, 0, 0, 0)'
      : props.rowClr
      ? props.rowClr
      : 'rgba(41, 38, 38, 0.897)'};

  @media (max-width: 540px) {
    height: 50px;
    width: 350px;
    margin: 10px;
  }

  @media (max-width: 430px) {
    height: 50px;
    width: 250px;
    margin: 10px;
  }

  &:hover {
    transform: translateY(-3px);
    box-shadow: 2px 10px 20px rgb(0, 0, 0);
  }

  ${(props) =>
    props.borderClr &&
    css`
      border: 1px solid ${props.borderClr};
    `}
`;

const LinkImage = styled.div`
  background-size: cover;
  padding: 5px;
  height: 30px;
  width: 30px;
  margin: 10px auto 10px 10px;
  border-radius: 10px;

  @media (max-width: 430px) {
    height: 20px;
    width: 20px;
    border-radius: 5px;
  }
  ${(props) =>
    props.linkImg &&
    css`
      background-image: url(${props.linkImg});
    `}
`;

const Links = () => {
  const linkArray = [];

  for (const key of Object.keys(links)) {
    linkArray.push(links[key]);
  }

  const linksJsx = linkArray.map((link) => (
    <div key={link.linkUrl}>
      <Link
        href={link.linkUrl}
        transparency={transparency}
        borderClr={borderColor}
        rowClr={rowColor}
      >
        <LinkImage linkImg={link.linkImg}></LinkImage>
        <div className="link__description">{link.linkDesc}</div>
      </Link>
    </div>
  ));

  return <div>{linksJsx}</div>;
};

export default Links;
